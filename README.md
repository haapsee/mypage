# Mypage

[![License: ISC](https://img.shields.io/badge/License-ISC-blue.svg)](https://opensource.org/licenses/ISC)

MyPage is a WebCV mainly for myself.
Project got it's web design on 14.6.2022 and its design is running on kubernetes cluster [eemil.haapsaari.eu](https://eemil.haapsaari.eu).

## Target

Project is supposed to generate CV from different sources and display it using React or Angular.

## TODO

* Change frontend into react
* Create cron that saves project information into the database.
* Create backend service for receeiving data from database.
* Move all data into database.
* Create dashboard for editing data.
