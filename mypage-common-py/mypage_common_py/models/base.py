from sqlalchemy.orm import declarative_base
from sqlalchemy.dialects.postgresql import UUID
import sqlalchemy as sqla
import uuid


ModelBase = declarative_base()


class Base(ModelBase):
    id = sqla.Column(
        UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
    )

    def __repr__(self):
        return f'id: {self.id}'
