# from mypage_common_py.models.base import Base
# from sqlalchemy.dialects.postgresql import UUID
# from sqlalchemy.orm import relationship
# import sqlalchemy as sqla
# import enum
#
#
# class Profiency(enum.Enum):
#     ELEMENTARY = 'ELEMENTARY'
#     LIMITED_WORKING = 'LIMITED_WORKING'
#     PROFESSIONAL_WORKING = 'PROFESSIONAL_WORKING'
#     FULL_PROFESSIONAL = 'FULL_PROFESSIONAL'
#     NATIVE_OR_BILINGUAL = 'NATIVE_OR_BILINGUAL'
#
#
# class LocalizedString(Base):
#     __tablename__ = 'localizedstrings'
#
#     en_US = sqla.Column(sqla.String())
#     fi_FI = sqla.Column(sqla.String())
#     preferredlocale = sqla.Column(sqla.String())
#
#
# class LinkedIn(Base):
#     __tablename__ = 'linkedin'
#
#     linkedinid = sqla.Column(sqla.String())
#     firstname_id = sqla.Column(UUID, sqla.ForeignKey('localizedstrings.id'))
#     firstname = relationship('LocalizedString')
#     lastname_id = sqla.Column(UUID, sqla.ForeignKey('localizedstrings.id'))
#     lastname = relationship('LocalizedString')
#     headline_id = sqla.Column(UUID, sqla.ForeignKey('localizedstrings.id'))
#     headline = relationship('LocalizedString')
#     profilepicture = sqla.Column(sqla.String())
#
#
# class LinkedInLanguage(Base):
#     __tablename__ = 'linkedinlanguages'
#
#     profiency = sqla.Column(sqla.Enum(Profiency))
#     language_id = sqla.Column(UUID, sqla.ForeignKey('localizedstrings.id'))
#     language = relationship('LocalizedString')
