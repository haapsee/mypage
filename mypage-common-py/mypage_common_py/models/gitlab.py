from mypage_common_py.models.base import Base
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
import sqlalchemy as sqla


class Project(Base):
    __tablename__ = 'gitlab_projects'
    gitlabid = sqla.Column(sqla.String)
    name = sqla.Column(sqla.String)
    created_at = sqla.Column(sqla.DateTime(timezone=True))
    tags = relationship('Tag', cascade='all, delete-orphan')
    topics = relationship('Topic', cascade='all, delete-orphan')


class Topic(Base):
    __tablename__ = 'gitlab_topics'
    value = sqla.Column(sqla.String)
    project_id = sqla.Column(UUID)
    project = relationship('Project')
