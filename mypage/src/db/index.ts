// https://github.com/BerkeKaragoz/nextjs-muiv5-typeorm-ts-boilerplate/blob/93ba20d72ace9ed649e4e2956ccf99f568a91ca4/src/database/index.ts

import "reflect-metadata";
import { Connection, getConnectionManager } from "typeorm";
import { User } from "./entities/user";

const options = {
  default: {
    type: "postgres",
    host: process.env.DB_HOST || "postgres",
    port: process.env.DB_PORT || "5432",
    username: process.env.DB_USER || "postgres",
    password: process.env.DB_PASSWORD || "postgres",
    database: process.env.DB_DB || "mypage",
    synchronize: process.env.NODE_ENV !== "production",

    entities: [
      User,
    ],
  },
};

const entitiesHasChanged = (previousEntities: any[], newEntities: any[]): boolean => {
  if (previousEntities.length !== newEntities.length) return true;
  return previousEntities.map((v: any, i: number, _a: any[]) => {
    return v == newEntities[i];
  }).includes(false);
}

const updateConnectionEntities = async (
  connection: Connection,
  entities: any[],
) => {
  // @ts-ignore
  if (!entitiesHasChanged(connection.options.entities, entities)) return;

  // @ts-ignore
  connection.options.entities = entities;

  // @ts-ignore
  connection.buildMetadatas();

  if (connection.options.synchronize) {
    await connection.synchronize();
  }
};

export const ensureConnection = async (
  name: string = "default",
): Promise<Connection> => {
  const connectionManager = getConnectionManager();

  if (connectionManager.has(name)) {
    const connection = connectionManager.get(name);

    if (!connection.isConnected) {
      await connection.connect();
    }

    if (process.env.NODE_ENV !== "production") {
      // @ts-ignore
      await updateConnectionEntities(connection, options[name].entities);
    }

    return connection;
  }

  // @ts-ignore
  return await connectionManager.create({ name, ...options[name] }).connect();
};

export default ensureConnection;
