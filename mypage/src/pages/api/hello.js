// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import User from '../../db/entities/user';
import { ensureConnection } from '../../db';

ensureConnection();

export default function handler(req, res) {
  res.status(200).json({ name: 'John Doe' })
}
