import Link from 'next/link';
import styles from '../styles/public.module.css';
import Main from '../components/main';
import Chip from '../components/chip';
import Content from '../components/content';
import Header from '../components/header';
import HeaderTitle from '../components/header-title';
import Nav from '../components/nav';
import NavItem from '../components/nav-item';
import Section from '../components/section';
import ProfileCard from '../components/profile-card';
import ProfileCardImage from '../components/profile-card-image';
import ProfileCardTitle from '../components/profile-card-title';
import ProfileCardExtension from '../components/profile-card-extension';
import ProfileDescription from '../components/profile-description';
import Timeline from '../components/timeline';
import TimelineItem from '../components/timeline-item';
import Language from '../components/language';
import Footer from '../components/footer';
import FooterColumn from '../components/footer-column';


export default function Home() {

  var chips = [
    "Cloud",
    "CI",
    "Docker",
    "DevOps",
    "Software architecture",
    "Software development",
    "Frontend",
    "Backend",
    "SQL",
    "Git",
  ];

  var profileDescription = {
    header: 'Hello.',
    pharagraps: [
      "I'm a full-stack developer with expertise in Cloud, " +
      "Continous integrations, backend and frontend " +
      "development using technologies such as Angular, " +
      "Python, Docker (-compose) and gitlab. " +
      "I'm accustomed to system integrations via APIs with " +
      "5+ years of experience in software development.",
      "I&apos;m persistent problem solver and that comes as my strength and " +
      "weakness. As a professional developer I like to deliver fully working, " +
      "tested, documented and easy to read code. As a " +
      "hobbyist developer I like to read  about new technologies and machine " +
      "learning."
    ]
  }

  var timelineitems = [
    {
      title: 'Codemate Ltd, Software engineer',
      start: 2022,
      end: 2022,
      description: 'Working on a python ticket system. Developed python code for database usage. Designed and developed continuous integration and docker updates into the project.'
    },
    {
      title: 'Codemate Ltd, Software engineer',
      start: 2021,
      end: 2022,
      description: 'Architecture design and lead development of recommendation app for beverages. Created small team of coworkers without project at the time and worked as a lead developer/architect for this project. This application has nestjs as a backend and react as a frontend. As a database solution I decided to use relational database called postgresql.'
    },
    {
      title: 'Visma Oy, Software engineer',
      start: 2021,
      end: 2021,
      description: 'Web application using multiple source system that needed to be integrated into single system. Java was used as a backend with spring boot framework and Angular was used as a frontend. My responsibilities (mostly) in the project was development with angular and java.'
    },
    {
      title: 'Visma Oy, jr. Software developer',
      start: 2018,
      end: 2021,
      description: 'Working with a project for financial solutions. Web application to replace current applications used by the company. As a backend we had microservice containing technologies such as python, flask, django, sqlalchemy and jsreport. On the frontend side we were using Angular with angular material.'
    },
    {
      title: 'Trivore Oy, jr. Software developer',
      start: 2017,
      end: 2018,
      description: 'Java development with Vaadin, spring boot and Android studio.'
    },
    {
      title: 'Taloustieto Irina Eklund, Software developer',
      start: 2021,
      end: 2022,
      description: 'Design, architecture and development of web application that is used for economics studies. Application contains only frontend and it\'s developed using Angular framework. Customer wanted an application with three columns, hidable subtitles and background image on pc screen.'
    },
  ]

  return (
    <Main>
      <Header>
        <HeaderTitle><b>Eemil Haapsaari /</b> Software Developer</HeaderTitle>
        <Nav>
          <NavItem link="#aboutme">
            About me
          </NavItem>
          <NavItem link="#experience">
            Eperience
          </NavItem>
          <NavItem link="#languages">
            Languages
          </NavItem>
        </Nav>
      </Header>
      <Content>
        <Section flex={true} id="aboutme">
          <ProfileCard>
            <ProfileCardImage />
            <ProfileCardTitle>
              <h2>Eemil Haapsaari</h2>
              <h3>Software Developer</h3>
            </ProfileCardTitle>
            <ProfileCardExtension>
              {chips.map((chip, i) => <Chip key={i}>{chip}</Chip>)}
            </ProfileCardExtension>
          </ProfileCard>
          <ProfileDescription>
            <h2>{profileDescription.header}</h2>

            {profileDescription.pharagraps.map((data, i) => {
              return <p key={i}>{data}</p>
            })}
          </ProfileDescription>
        </Section>
        <Section id="experience">
          <h2 className={styles.center}>Working experience</h2>
          <Timeline>
            {timelineitems.map(({ start, end, title, description }, i) => {
              return (
                <TimelineItem start={start} end={end} title={title} key={i}>
                  {description}
                </TimelineItem>
              )
            })}
          </Timeline>
        </Section>
        <Section id="languages">
          <h2 className={styles.center}>Languages</h2>
          <Language>
            <span>Finnish</span><span>Native</span>
          </Language>
          <Language>
            <span>English</span><span>Professional Working</span>
          </Language>
          <Language>
            <span>Swedish</span><span>Elementary</span>
          </Language>
        </Section>
        <Footer>
          <FooterColumn>
            <div>
              <b>Email: </b>firstname@lastname.fi
            </div>
            <div>
              <b>LinkedIN: </b>
              <Link href="https://www.linkedin.com/in/eemil-haapsaari/">
                <a>Eemil Haapsaari</a>
              </Link>
            </div>
          </FooterColumn>
          <FooterColumn>
          </FooterColumn>
          <FooterColumn>
            <div>&copy; Eemil Haapsaari</div>
            <div>
              <a href="https://gitlab.com/haapsee/mypage">Project repository</a>
            </div>
          </FooterColumn>
        </Footer>
      </Content>
    </Main>
  )
}

// export async function getStaticProps() {
//   const res = await fetch('http://192.168.2.215:8080/person.json');
//   const data = await res.json();
//
//   return {
//     props: data
//   }
// }
