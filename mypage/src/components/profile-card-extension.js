import publicStyles from '../styles/public.module.css';
import styles from '../styles/profile-card-extension.module.css';

export default function ProfileCardExtension({ children }) {
  return (
    <div className={`${styles.profile_card_extension} ${publicStyles.center}`}>
      {children}
    </div>
  );
}
