import styles from '../styles/profile-card-title.module.css';

export default function ProfileCardTitle({ children }) {
  return (
    <div className={`${styles.profile_card_title} `}>
      {children}
    </div>
  );
}
