import publicStyles from '../styles/public.module.css';
import styles from '../styles/section.module.css';

export default function Section({ children, id, flex }) {
  return (
    <div id={id} className={`${styles.section} ${flex ? publicStyles.flex : ''}`}>
      {children}
    </div>
  );
}
