import styles from '../styles/profile-card.module.css';

export default function ProfileCard({ children }) {
  return <div className={styles.profile_card}>{children}</div>;
}
