import styles from '../styles/timeline-item.module.css';

export default function TimelineItem({ children, start, end, title }) {
  return (
    <div className={`${styles.timeline_item}`}>
      <svg height="20" width="20">
        <circle cx="10" cy="10" r="8" stroke="black" strokeWidth="3" fill="green"></circle>
      </svg>
      <p className={styles.timeline_date}>{start}-{end}</p>
      <p className={styles.timeline_title}>{title}</p>
      <p className={styles.timeline_description}>{children}</p>
    </div>
  );
}
