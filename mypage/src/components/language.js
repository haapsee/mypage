import styles from '../styles/language.module.css';

export default function Language({ children }) {
  return (
    <div className={`${styles.language}`}>
      {children}
    </div>
  );
}
