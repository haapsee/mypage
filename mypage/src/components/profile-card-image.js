import Image from 'next/image';
import styles from '../styles/profile-card-image.module.css';

export default function ProfileCardImage() {
  return (
    <div className={styles.profile_image}>
      <Image src="/profile.jpg" width="1000" height="1000" alt="profile_image" />
    </div>
  );
}
