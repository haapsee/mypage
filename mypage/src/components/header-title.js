import styles from '../styles/header-title.module.css';

export default function HeaderTitle({ children }) {
  return <span className={styles.header_title}>{children}</span>;
}
