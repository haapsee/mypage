import styles from '../styles/footer-column.module.css';

export default function FooterColumn({ children }) {
  return (
    <div className={`${styles.footer_column}`}>
      {children}
    </div>
  );
}
