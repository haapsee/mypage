import React from 'react';
import styles from '../styles/nav.module.css';

export default function Nav({ children }) {
  const [navVisibility, setNavVisibily] = React.useState('hidden');

  const onClick = () => setNavVisibily(navVisibility == '' ? 'hidden' : '');
  const reverseNavVisibility = navVisibility == '' ? 'hidden' : 'visible';

  return (
    <>
      <nav className={`${styles.nav_mobile} ${navVisibility}`}>
        {children}
      </nav>
      <div className={styles.container}>
        <nav className={`${styles.nav}`}>{children}</nav>
        <button className={styles.nav_button} onClick={onClick}>
          <svg viewBox="0 0 24 24" className={reverseNavVisibility}>
            <line x1="0" x2="24" y1="3" y2="3" stroke="black" />
            <line x1="0" x2="24" y1="12" y2="12" stroke="black" />
            <line x1="0" x2="24" y1="21" y2="21" stroke="black" />
          </svg>
          <svg viewBox="0 0 24 24" className={navVisibility}>
            <line x1="3" x2="21" y1="3" y2="21" stroke="black" />
            <line x1="3" x2="21" y1="21" y2="3" stroke="black" />
          </svg>
        </button>
      </div>
    </>
  );
}
