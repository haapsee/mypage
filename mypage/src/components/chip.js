import publicStyles from '../styles/public.module.css';
import styles from '../styles/chip.module.css';

export default function Chip({ children }) {
  return (
    <div className={`${styles.chip} ${publicStyles.center}`}>
      {children}
    </div>
  );
}
