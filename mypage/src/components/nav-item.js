import Link from 'next/link';
import styles from '../styles/nav-item.module.css';

export default function NavItem({ children, link }) {
  return (
    <Link href={link}>
      <a className={styles.nav_item}>{children}</a>
    </Link>
  );
}
