import styles from '../styles/profile-description.module.css';

export default function ProfileDescription({ children }) {
  return (
    <div className={`${styles.profile_description}`}>
      {children}
    </div>
  );
}
